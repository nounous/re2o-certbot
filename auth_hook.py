#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright © 2019 Alexandre Iooss

"""
Hook for Let's Encrypt Certbot
It places a TXT entry in Re2o DNS service.
"""

import os
from configparser import ConfigParser

from re2oapi import Re2oAPIClient


def find_extension_re2o(api_client, name):
    """Find the extension instance in Re2o"""
    extensions = api_client.list('machines/extension')
    for e in extensions:
        if e['name'] == name[:-1]:
            return e
    return None


def get_txt():
    """Get the TXT entry to add to Re2o"""
    # Get environment variables passed by certbot
    domain = os.environ.get('CERTBOT_DOMAIN')
    validation = os.environ.get('CERTBOT_VALIDATION')

    # Assert that all environment variables are defined
    assert domain and validation

    # Build extension and challenge domain
    extension_name = '.' + domain
    challenge_domain = '_acme-challenge.' + domain

    return extension_name, challenge_domain, validation


# Get Re2o API client
config = ConfigParser()
config.read('config.ini')
api_hostname = config.get('Re2o', 'hostname')
api_username = config.get('Re2o', 'username')
api_password = config.get('Re2o', 'password')
api_client = Re2oAPIClient(api_hostname, api_username, api_password)

# Get DNS TXT entry
extension, field1, field2 = get_txt()

# Find corresponding extension in Re2o
zone = find_extension_re2o(api_client, extension)
assert zone, 'The extension need to be defined in Re2o'
print("Chosen zone:", zone)

# Add TXT record
target = api_client.get_url_for('machines/txt')
api_client.post(target, data={
    'zone': zone,
    'field1': field1,
    'field2': field2,
})

# Hooks pour Let's Encrypt Certbot

Ce dépôt contient :

 * Un script `auth_hook.py` permettant l'ajout de l'entrée TXT dans Re2o puis d'attendre la propagation du DNS ;
 * Un script `cleanup_hook.py` permettant la suppression de l'entrée TXT dans Re2o.

## Mise en place pour un certificat Let's Encrypt wildcard

Il faut une version récente de CertBot, vérifiez que votre Debian est >= Stretch et bien à jour.

D'abord on clone ce dépôt dans `/var/local/re2o-services/certbot`.
On vérifie que l'on a bien les secrets pour discuter avec Re2o dans `/var/local/re2o-services/config.ini`.

Ensuite on initialise une première fois la procédure de génération de certification

```
certbot certonly --manual --preferred-challenges dns --manual-auth-hook "/var/local/re2o-services/certbot/auth_hook.py" --manual-cleanup-hook "/var/local/re2o-services/certbot/cleanup_hook.py" --cert-name crans.org -d 'crans.org,*.crans.org'
```

Dans le futur il suffira d'éxécuter `certbot renew` ou d'attendre que le cron fasse son job pour renouveller le certificat.
